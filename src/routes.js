import Dashboard from "views/Dashboard.js";
import BarChartRace from "views/BarChartRace.js";
import Maps from "views/Map.js";


var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/maps",
    name: "World Map",
    icon: "nc-icon nc-pin-3",
    component: Maps,
    layout: "/admin",
  },
  {
    path: "/BarChartRace",
    name: "BarChart Race",
    icon: "nc-icon nc-tile-56",
    component: BarChartRace,
    layout: "/admin",
  },
 
 
];
export default routes;
